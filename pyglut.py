import sys
import OpenGL
import OpenGL.GL as gl
import OpenGL.GLUT as glut
import OpenGL.GLU as glu
import random

VISUAL_DEBUG = False

PLAYER_MISSILES_SIZE  = 10
ENEMIES_SIZE          = 15
ENEMIES_MISSILES_SIZE = 5

class Point:
  def __init__(self, x, y):
    self.x = x
    self.y = y
  def numberOf(points):
    return len(list(filter(lambda p : p != None, points)))
class Dimension:
  def __init__(self, width, height):
    self.width  = width
    self.height = height
class Rectangle:
  def __init__(self, point, dimension):
    self.point     = point
    self.dimension = dimension
  def contains(self, point):
    return True if point.x >= self.point.x and point.x <= self.point.x + self.dimension.width and point.y <= self.point.y and point.y >= self.point.y - self.dimension.height else False

class Thing:
  def __init__(self, timeout = 0, screen = None, player = None, player_speed = 0.0, player_missiles = None, player_missilesSpeed = 0.0, enemies = None, enemies_speed = 0.0, enemies_offset = 0, enemies_direction = 1, enemies_perRow = 0, enemies_verticalSpacing = 0, enemies_missiles = None, enemies_missilesSpeed = 0.0):
    self.timeout                 = timeout
    self.screen                  = screen
    self.player                  = player
    self.player_speed            = player_speed
    self.player_missiles         = player_missiles
    self.player_missilesSpeed    = player_missilesSpeed
    self.enemies                 = enemies
    self.enemies_speed           = enemies_speed
    self.enemies_offset          = enemies_offset
    self.enemies_direction       = enemies_direction
    self.enemies_perRow          = enemies_perRow
    self.enemies_verticalSpacing = enemies_verticalSpacing
    self.enemies_missiles        = enemies_missiles
    self.enemies_missilesSpeed   = enemies_missilesSpeed

class Game:

  def main():
    glut.glutInit(sys.argv);
    game = Game()
    game.t = Thing(timeout = 30, screen = game.getScreenSize())
    game.initPlayer()
    game.initEnemies()
    glut.glutInitDisplayMode(glut.GLUT_DEPTH | glut.GLUT_DOUBLE | glut.GLUT_RGBA)
    glut.glutInitWindowPosition(0, 0)
    glut.glutInitWindowSize(game.t.screen.width, game.t.screen.height)
    glut.glutCreateWindow("glut test")
    glut.glutDisplayFunc(game.renderScene)
    glut.glutReshapeFunc(game.changeSize)
    glut.glutKeyboardFunc(game.keyboard)
    glut.glutSpecialFunc(game.altKeyboard)
    glut.glutTimerFunc(0, game.action, 0)
    glut.glutMainLoop()

    return 1

  def initPlayer(self):
    self.t.player_speed         = 0.03
    self.t.player_missilesSpeed = 0.03
    self.t.player               = Point(0.0, -0.7)
    self.t.player_missiles      = []

    for i in range(PLAYER_MISSILES_SIZE):
      self.t.player_missiles.append(None)

  def initEnemies(self):
    self.t.enemies_speed           = 0.01
    self.t.enemies_missilesSpeed   = 0.03
    self.t.enemies_offset          = 0.0
    self.t.enemies_direction       = 1     #move to the right
    self.t.enemies_perRow          = 5
    self.t.enemies_verticalSpacing = 4.0

    leftOffset = -1.0
    width      = 2.0                       #center them with some room on the side

    self.t.enemies = []
    for i in range(ENEMIES_SIZE):
      pos = (i / (self.t.enemies_perRow / width))
      x = pos % width + leftOffset
      y = pos / width
      y = 1 - (y / self.t.enemies_verticalSpacing)
      self.t.enemies.append(Point(x, y))

    self.t.enemies_missiles = []
    for i in range(ENEMIES_MISSILES_SIZE):
      self.t.enemies_missiles.append(None)

  def action(self, value):
    self.moveMissiles()
    self.moveEnemies()
    self.detectCollision()
    glut.glutPostRedisplay()
    glut.glutTimerFunc(self.t.timeout, self.action, 0)


  def getScreenSize(self):
    return Dimension(glut.glutGet(glut.GLUT_SCREEN_WIDTH), glut.glutGet(glut.GLUT_SCREEN_HEIGHT))

  def changeSize(self, w, h):
    print(f"width: {w}, height: {h}")
    if h == 0:
      h = 1
    ratio =  w * 1.0 / h
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.glViewport(0, 0, w, h)
    glu.gluOrtho2D(-1.0 * ratio, 1.0 * ratio, -1.0, 1.0)

  def renderScene(self):
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    gl.glBegin(gl.GL_TRIANGLES)                                   #player
    gl.glVertex2f( 0.00 + self.t.player.x,  0.00 + self.t.player.y)
    gl.glVertex2f( 0.03 + self.t.player.x, -0.03 + self.t.player.y)
    gl.glVertex2f(-0.03 + self.t.player.x, -0.03 + self.t.player.y)
    gl.glEnd()

    for i in range(PLAYER_MISSILES_SIZE):
      if(None != self.t.player_missiles[i]):
        gl.glBegin(gl.GL_TRIANGLES)                                #missile
        gl.glVertex2f( 0.00 + self.t.player_missiles[i].x,  0.00 + self.t.player_missiles[i].y)
        gl.glVertex2f( 0.01 + self.t.player_missiles[i].x, -0.01 + self.t.player_missiles[i].y)
        gl.glVertex2f(-0.01 + self.t.player_missiles[i].x, -0.01 + self.t.player_missiles[i].y)
        gl.glEnd()

    for i in range(ENEMIES_SIZE):
      if(None != self.t.enemies[i]):
        #draw hit box
        if(VISUAL_DEBUG):
          gl.glGetFloatv(gl.GL_CURRENT_COLOR, currentColor) #how do in-out parameters work?
          gl.glColor3f(1.0, 0.0, 0.0)
          gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)
          hitBox = getHitBox(self.t.enemies[i])
          gl.glRectf(hitBox.point.x + self.t.enemies_offset, hitBox.point.y, hitBox.point.x + hitBox.dimension.width + self.t.enemies_offset, hitBox.point.y - hitBox.dimension.height)
          gl.glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
          gl.glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3])

        gl.glBegin(gl.GL_TRIANGLES)                                  #enemies
        gl.glVertex2f(( 0.00 + self.t.enemies[i].x) + self.t.enemies_offset,  0.00 + self.t.enemies[i].y)
        gl.glVertex2f(( 0.03 + self.t.enemies[i].x) + self.t.enemies_offset, -0.03 + self.t.enemies[i].y)
        gl.glVertex2f((-0.03 + self.t.enemies[i].x) + self.t.enemies_offset, -0.03 + self.t.enemies[i].y)
        gl.glEnd()

    for i in range(ENEMIES_MISSILES_SIZE):
      if(None != self.t.enemies_missiles[i]):
        gl.glBegin(gl.GL_TRIANGLES)                                  #enemiesMissiles
        gl.glVertex2f( 0.00 + self.t.enemies_missiles[i].x,  0.00 + self.t.enemies_missiles[i].y)
        gl.glVertex2f( 0.01 + self.t.enemies_missiles[i].x, -0.01 + self.t.enemies_missiles[i].y)
        gl.glVertex2f(-0.01 + self.t.enemies_missiles[i].x, -0.01 + self.t.enemies_missiles[i].y)
        gl.glEnd()
    glut.glutSwapBuffers()

  def keyboard(self, key, x, y):
    if ord(' ') == key[0]:
      for i in range(PLAYER_MISSILES_SIZE):
        if None == self.t.player_missiles[i]:
          self.t.player_missiles[i] = Point(self.t.player.x, self.t.player.y)
          break

  def altKeyboard(self, key, x, y):
    if glut.GLUT_KEY_LEFT == key:
      self.t.player.x -= self.t.player_speed
    elif glut.GLUT_KEY_RIGHT == key:
      self.t.player.x += self.t.player_speed
    elif glut.GLUT_KEY_UP == key:
      self.t.player.y += self.t.player_speed
    elif glut.GLUT_KEY_DOWN == key:
      self.t.player.y -= self.t.player_speed
    else:
      print(f"key: {key}")

  def moveMissiles(self):
    #player missile
    for i in range(PLAYER_MISSILES_SIZE):
      if None == self.t.player_missiles[i]:
        continue
  
      if self.t.player_missiles[i].y < 1.0:
        self.t.player_missiles[i].y += self.t.player_missilesSpeed
      else:
        self.t.player_missiles[i] = None
  
    for i in range(ENEMIES_MISSILES_SIZE):
      r = int(random.random() * ENEMIES_SIZE) #pick a random enemy
      if None != self.t.enemies[r] and None == self.t.enemies_missiles[i]:
        self.t.enemies_missiles[i]    = Point(self.t.enemies[r].x + self.t.enemies_offset, self.t.enemies[r].y)
  
      if None != self.t.enemies_missiles[i]:
        if self.t.enemies_missiles[i].y > -1.0:
          self.t.enemies_missiles[i].y -= self.t.enemies_missilesSpeed
        else:
          self.t.enemies_missiles[i] = None

  def moveEnemies(self):
    #TODO: calculate boundries
    if self.t.enemies_offset <= -0.75 or self.t.enemies_offset >= 0.75:
      self.t.enemies_direction *= -1

    self.t.enemies_offset += self.t.enemies_speed * self.t.enemies_direction

  def detectCollision(self):
    result = False
    #player missile
    for i in range(ENEMIES_SIZE):
      for j in range(PLAYER_MISSILES_SIZE):
        if None == self.t.enemies[i] or None == self.t.player_missiles[j]:
          continue

        p = Point(self.t.enemies[i].x + self.t.enemies_offset, self.t.enemies[i].y)
        hitBox = self.getHitBox(p)
        if hitBox.contains(self.t.player_missiles[j]):
          self.t.enemies[i] = None
          if 0 == Point.numberOf(self.t.enemies):
            #doWin(screenSize)
            print("you win")
            glut.glutLeaveMainLoop()

          self.t.player_missiles[j] = None
          result = True

    #enemies missile
    for i in range(ENEMIES_MISSILES_SIZE):
      if None == self.t.enemies_missiles[i]:
        continue

      hitBox = self.getHitBox(self.t.player)
      if hitBox.contains(self.t.enemies_missiles[i]):
        self.t.enemies_missiles[i] = None
        #doLose(screenSize)
        print("you lose")
        glut.glutLeaveMainLoop()

    return result

#/*private*/
  def getHitBox(self, point):
    padding            = 0.03 #TODO: get rid of the magic
    return Rectangle(Point(point.x - padding, point.y + padding), Dimension(padding * 2, padding * 2))

if __name__ == "__main__":
  Game.main()
